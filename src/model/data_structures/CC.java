package model.data_structures;

import java.util.Iterator;

public class CC< K,V > 
{

	private HashTableChaining<K, Vertex<K,V>> subVs;
	private Vertex<K,V> comienzo; 
	private K key;
	private int size;

	public CC( DirectedGraph<K,V> grafo, Vertex<K,V> raiz)
	{
		size = 0;
		comienzo = raiz;
		key = comienzo.getKey();
		subVs = new HashTableChaining<K, Vertex<K,V>>();
		subVs.put(comienzo.getKey(), comienzo);
	}

	public K getKey()
	{
		return key;
	}

	public Vertex<K,V> getStartVertex()
	{
		return comienzo;
	}

	public HashTableChaining<K, Vertex<K,V>> getComponent()
	{
		return subVs;
	}

	public void build()
	{
		Iterator<Vertex<K,V>> iterator = comienzo.getAdjacents().values();
		while(iterator.hasNext())
		{
			Vertex<K,V> ver = iterator.next();
			
			if(subVs.get(ver.getKey()) == null)
			{
				subVs.put(ver.getKey(), ver);
				build(ver);
			}
		}
		
		size = subVs.size();
	}

	private void build(Vertex<K,V> vertice)
	{
		Iterator<Vertex<K,V>> iterator = vertice.getAdjacents().values();
		
		while(iterator.hasNext())
		{
			Vertex<K,V> ver = iterator.next();
			
			if(subVs.get(ver.getKey()) == null)
			{
				subVs.put(ver.getKey(), ver);
				build(ver);
			}
		}
	}

	public int size()
	{
		return size;
	}


}