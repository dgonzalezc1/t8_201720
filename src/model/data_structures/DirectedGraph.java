package model.data_structures;

import java.util.Iterator;

public class DirectedGraph<K,V > 
{

	private HashTableChaining<K, Vertex<K,V>> vs;
	private RingList<Edge<K,V>> edges;
	private int numEdges;
	private int numVertex;
	private RingList<Vertex<K,V>> raices;
	private Vertex<K,V> primeraRaiz;

	public DirectedGraph()
	{
		vs = new HashTableChaining<K, Vertex<K,V>>();
		edges = new RingList<Edge<K,V>>();
		this.numEdges = 0;
		this.numVertex = 0;
	}

	public int Edges()
	{
		return numEdges;
	}

	public int Vertex()
	{
		return numVertex;
	}

	public RingList<Edge<K,V>> getEdges()
	{
		return edges;
	}

	public HashTableChaining<K, Vertex<K,V>> getVertex()
	{
		return vs;
	}

	public Vertex<K,V> darVerticeKey(K key)
	{
		return vs.get(key);
	}

	public Iterable<V> getValues()
	{
		RingList<V> rta = new RingList<V>();
		Iterator iterador = vs.keys();
		int numCosas = vs.size();
		for (int i = 0; i < numCosas; i++) 
		{

		}
		int i = 0;
		while(iterador != null && iterador.hasNext()&& i <= numCosas)
		{
			V actual = (V) iterador.next();
			if(actual != null)
			{
				rta.addFirst(actual);
			}
			i++;
		}


		return rta;
	}

	public Vertex<K,V> darVerticePos(int pos)
	{
		Vertex<K, V> resp = null;
		Iterator<K> iterador = vs.keys();
		int contador = 0;

		while(iterador.hasNext())
		{
			contador++;
			Vertex<K, V> vertice = vs.get(iterador.next());

			if(vs!=null && contador == pos)
			{
				resp = vertice;
			}

		}

		return resp;
	}


	public void addVertex( K key, V value)
	{
		if(!vs.contains(key))
		{
			Vertex<K,V> vrt = new Vertex<K,V>(key, value);
			this.numVertex++;
			vs.put(vrt.getKey(), vrt);
		}

	}

	public void addEdge(K source, K end, double weight ) throws Exception
	{
		if(!vs.contains(source))
		{
			throw new Exception("El vertice source no existe");
		}

		if(!vs.contains(end))
		{
			throw new Exception("El vertice end no existe");
		}

		if(vs.contains(source) && vs.contains(end))
		{
			Edge<K,V> edge = new Edge<K,V>(vs.get(source), vs.get(end), true);
			edge.setWeight(weight);
			edges.addLast(edge);
		}
	}


	/**
	 * Returns the vertices adjacent to vertex {@code v}.
	 *
	 * @param  v the vertex
	 * @return the vertices adjacent to vertex {@code v}, as an iterable
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public Iterable adj(int v) {
		validateVertex(v);
		return  (Iterable) vs.getValuePos(v).getAdjacents();
	}

	private void validateVertex(int v) {
		if (v < 0 || v >= numVertex)
			throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (numVertex-1));
	}

	public boolean isEmpty()
	{
		if(numVertex==0)
		{
			return true;
		}
		else
			return false;
	}

	public void crearRaices()
	{
		Vertex<K,V> vertice = null;
		Iterator<Vertex<K,V>> iterador = vs.values();
		
		while( iterador.hasNext())
		{
			vertice = vs.get(iterador.next().getKey());
			raices.addLast(vertice);
			
		}

		primeraRaiz = (Vertex<K,V>) raices.getElement(0);
	}	
	
	
	public Vertex<K,V> getFirstRoot(){

		return primeraRaiz;
	}

	public RingList<Vertex<K,V>> darRaices()
	{
		return raices;
	}

	public String toString() 
	{
		StringBuilder s = new StringBuilder();
		s.append(numVertex + " vertices, " + numEdges + " edges " + "\n");
		for (int v = 0; v < numVertex; v++) 
		{
			s.append(String.format("%d: ", v));

			for (K w : darVerticePos(v).getAdjacents().keysIterable()) 
			{
				s.append(String.format("%d ", darVerticePos(v).getAdjacents().get(w)));
			}

			s.append("\n");
		}
		return s.toString();
	}
}
