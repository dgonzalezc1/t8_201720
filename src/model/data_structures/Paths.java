package model.data_structures;

import java.util.Iterator;

public class Paths<K,V>
{

	private int size;
	private HashTableChaining<K, Vertex<K,V>> path;
	private DirectedGraph<K,V> global;
	private HashTableChaining<K, Vertex<K,V>> comparation;
	private K source;
	private K end;

	public Paths(K start,K stop, DirectedGraph<K,V> graph) throws Exception{
		size = 0;
		path = new HashTableChaining<K, Vertex<K,V>>(); 
		comparation = new HashTableChaining<K, Vertex<K,V>>();
		global = graph;
		source = start;
		end = stop;
		path.put(source, global.getVertex().get(source));
		comparation.put(source, global.getVertex().get(source));
		build();
	}

	public int size(){
		return size;
	}

	public HashTableChaining<K, Vertex<K,V>> getPath()
	{
		return path;
	}

	public void build() throws Exception
	{

		if(global.getVertex().get(source).getAdjacents().get(end) != null)
		{
			path.put(end, global.getVertex().get(end));

		}
		else{
			helper(global.getVertex().get(source));
			if(comparation.size() < path.size()){
				path = comparation;
			}
		}
		size = path.size();

	}

	private void helper(Vertex<K,V> temp)
	{
		Iterator<Vertex<K,V>> iterador = temp.getAdjacents().values();

		if(iterador.hasNext())
		{
			Vertex<K,V> var = iterador.next();

			if(var.getKey().equals(end))
			{
				path.put(var.getKey(), var);
			}

			else
			{
				shortestPath(iterador);

				if(path.get(var.getKey()) == null)
				{
					path.put(var.getKey(), var);
					helper(var);
				}
			}
		}

	}

	private void shortestPath(Iterator<Vertex<K,V>> iter)
	{
		iter.next();

		if(iter.hasNext())
		{
			comparation.put(iter.next().getKey(), iter.next());

			Iterator<Vertex<K,V>> ite2 = iter.next().getAdjacents().values();

			if(ite2.next() == null) return;

			Vertex<K,V> var = ite2.next();

			if(var.getKey().equals(end)){
				comparation.put(var.getKey(), var);
			}

			else
			{
				if(comparation.get(var.getKey()) == null ){
					comparation.put(var.getKey(), var);
					shortestPath(iter);
				}
			}	
		}
	}
}
