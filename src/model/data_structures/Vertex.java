package model.data_structures;



/*
 * Clase para un vertice
 */ 
public class Vertex<K, V> 
{

	private K key;
	private V value;
	private HashTableChaining<K, Vertex<K,V>> adjacentVertex;

	public Vertex(K pkey, V pval)
	{
		key = pkey;
		value = pval;
		adjacentVertex = new HashTableChaining<K, Vertex<K,V>>();
	}

	public K getKey()
	{
		return key;
	}

	public V getValue()
	{
		return value;
	}

	public HashTableChaining<K, Vertex<K,V>> getAdjacents()
	{
		return adjacentVertex;
	}

	public void addAdjacent(Vertex<K,V> v)
	{
		adjacentVertex.put(v.getKey(), v);
	}

}
