package model.data_structures;


import model.data_structures.Vertex;

/*
 * Clase para denominar un arco
 */
public class Edge<K,V>
{

	private Vertex<K,V> end;
	private Vertex<K,V> source;
	private boolean directed;
	private double weight;

	public Edge(Vertex<K,V> e, Vertex<K,V> s, boolean d)
	{
		weight = 0;
		end = e;
		source = s;
		directed = d;

		if(d=false)
		{
			source.addAdjacent(end);
			end.addAdjacent(source);
		}

		else 
			source.addAdjacent(end);	
	}

	public Vertex<K,V> End()
	{
		return end;
	}

	public Vertex<K,V> Source()
	{
		return source;
	}

	public double getWeight()
	{
		return weight;
	}

	public void setWeight(double w)
	{
		this.weight = w;
	}

	public boolean isDirected()
	{
		return directed;
	}
}