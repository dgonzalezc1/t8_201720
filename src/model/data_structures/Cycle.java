package model.data_structures;

import java.util.Iterator;

public class Cycle<K,V> 
{

	private HashTableChaining<K, Vertex<K,V>> cycle;
	private DirectedGraph<K,V> global;
	private int size;
	
	public Cycle(DirectedGraph<K,V> graph) throws Exception
	{
		size = 0;
		cycle = new HashTableChaining<K, Vertex<K,V>>();
		global = graph;
		this.build();
	}
	
	public HashTableChaining<K, Vertex<K,V>> getCycle()
	{
		return cycle;
	}
	
	public int size()
	{
		return size;
	}
	
	
	public void build() throws Exception
	{
		boolean si = false;
		
		Iterator<Vertex<K,V>> iter = global.getVertex().values();
		
		while(iter.hasNext()&&!si)
		{
			si = hasCycle(iter.next().getKey(), iter.next());
		}
		
		if(!si)
		{
			throw new Exception("El grafo no tiene ciclos");
		}
		size = cycle.size();
	}
	
	
	private boolean hasCycle(K key, Vertex<K,V> temp)
	{
		boolean hasCycle = false;
		
		Iterator<Vertex<K,V>> iter = temp.getAdjacents().values();
		
		while(iter.hasNext() && !hasCycle) 
		{
			Vertex<K,V> var = iter.next();
			
			if(var.getKey().equals(key))
			{
				cycle.put(var.getKey(), var);
				hasCycle = true;
			}
			
			else if(var.getAdjacents().size() != 0 )
			{
				cycle.put(var.getKey(), var);
				hasCycle = hasCycle(key, var);
			}
		}
		
		if(!hasCycle) cycle = new HashTableChaining<K, Vertex<K,V>>();
		return hasCycle;
	}
}
