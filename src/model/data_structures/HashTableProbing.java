package model.data_structures;

import java.util.Iterator;

public class HashTableProbing<Key, Value> {
private static final int INIT_CAPACITY = 4;

private int cosas;
private int tamano;
private Key[] llaves;
private Value[] valores;

public HashTableProbing() {
    this(INIT_CAPACITY);
}
	
public HashTableProbing(int capacity) {
	tamano = capacity;
    cosas = 0;
    llaves = (Key[]) new Object[capacity];  //inicializa los arrreglos vacios
    valores = (Value[]) new Object[capacity];
}
	public int numCosas()
	{
		return cosas;
	}
	public int tamano()
	{
		return tamano;
	}
	
    public boolean isEmpty() 
    {
        return numCosas() == 0;
    }

    private int hash(Key key) 
    {
        return (key.hashCode() & 0x7fffffff) % tamano;
    }

    public Value get(Key key) 
    {
        if (key == null) throw new IllegalArgumentException("paramatro es null");
        for (int i = hash(key); llaves[i] != null; i = (i + 1) % tamano) //lo bota a la posicion correspondiente de acuerdo al hash de lallave, si ya esta llena pasa al siguiente
            if (llaves[i].equals(key))
                return valores[i];
        return null;
    }
    
    public boolean contains(Key key) {
        if (key == null) throw new IllegalArgumentException("parametro es null");
        return get(key) != null;
    }
    
    public void resize(int capacity) {
       HashTableProbing<Key, Value> temp = new HashTableProbing<Key, Value>(capacity);
        for (int i = 0; i < tamano; i++) {
            if (llaves[i] != null) {
                temp.put(llaves[i], valores[i]);  //anade todas las duplas a la nueva hashtable
            }
        }
        llaves = temp.llaves;
        valores = temp.valores;
        tamano  = temp.tamano;
    }
    
    public void put(Key key, Value val) {
        if (key == null) throw new IllegalArgumentException("parametro es null");

        if (val == null) {
            delete(key);
            return;
        }

        // double table size if 75% full
        if ((cosas/tamano)>=0.75) 
        {
        	resize(2*tamano);
        }
        int i;
        for (i = hash(key); llaves[i] != null; i = (i + 1) % tamano) {
            if (llaves[i].equals(key)) {
                valores[i] = val;
                return;
            }
        }
        llaves[i] = key;	//anade la llave a llaves y lo mismo con el valor
        valores[i] = val;
        cosas++;
    }
    
    public void delete(Key key) {
        if (key == null) throw new IllegalArgumentException("parametro es null");
        if (!contains(key)) return;

        // encuentra la posicion de la llave, si lo bota a una que ya esta suma uno y asi
        int i = hash(key);
        while (!key.equals(llaves[i])) {
            i = (i + 1) % tamano;
        }

        // borra valores asociado a la posicion encontrada
        llaves[i] = null;
        valores[i] = null;

        // rehash all keys in same cluster
        i = (i + 1) % tamano;
        while (llaves[i] != null) {
            // borra la llave y el valor, pero lo vuelve a meter
            Key   keyToRehash = llaves[i];
            Value valToRehash = valores[i];
            llaves[i] = null;
            valores[i] = null;
            tamano--;
            put(keyToRehash, valToRehash);
            i = (i + 1) % tamano;
        }

        cosas--;

        // halves size of array if it's 12.5% full or less
        if (cosas > 0 && cosas<= tamano/8) resize(tamano/2);

        assert check();
    }

    private boolean check() {

        // check that hash table is at most 50% full
        if ((cosas/tamano)>0.75) {
            System.err.println("Hash table size m = " + tamano + "; cosas n = " + cosas);
            return false;
        }

        // mira que todos los valores correspondan a su llave
        for (int i = 0; i < tamano; i++) {
            if (llaves[i] == null) continue;
            else if (get(llaves[i]) != valores[i]) {
                System.err.println("get[" + llaves[i] + "] = " + get(llaves[i]) + "; vals[i] = " + valores[i]);
                return false;
            }
        }
        return true;
    }
    public Iterator<Key> keys() {
        RingList<Key> queue = new RingList<Key>();
        for (int i = 0; i < tamano; i++)
            if (llaves[i] != null) queue.addLast(llaves[i]);
        return queue.iterator();
    }

}
