package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Iterator;

import model.data_structures.CC;
import model.data_structures.Cycle;
import model.data_structures.DirectedGraph;
import model.data_structures.HashTableChaining;
import model.data_structures.RingList;
import model.data_structures.Vertex;
import model.vo.VORoute;
import model.vo.VOStop;
import model.vo.VOStopTimes;
import model.vo.VOTrip;


public class STSManager 
{

	//------------------------------------
	// ATRIBUTOS
	//------------------------------------

	private HashTableChaining<String, VORoute> rutasChaining;

	private RingList<VOStopTimes> listaStopTimes;

	private HashTableChaining<String, VOTrip> viajesChaining;


	private HashTableChaining<String, VOStop> listaParadasChaining;

	private DirectedGraph<String, VOStop> grafoDirigido;


	//------------------------------------
	// METODOS DE LEER LOS ARCHIVOS
	//------------------------------------


	//Carga las rutas en tabla de hash chaining
	public void loadRoutes(String routesFile) 
	{
		try 
		{
			rutasChaining = new HashTableChaining<String, VORoute>(6007);


			BufferedReader br = new BufferedReader(new FileReader(routesFile));

			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");


				String route_color = null;
				String route_text_color = null;

				if(info.length==10)
				{
					route_color = info[7];

					route_text_color = info[8];
				}
				else
				{
					route_color="";

					route_text_color="";
				}


				VORoute ruta = new VORoute (info[0], info[1], info[2], info[3], info[4], info[5], info[6],  route_color, route_text_color);
				//System.out.println(ruta);


				rutasChaining.put(info[0], ruta);

				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 
	}


	//Carga los viajes en tabla de hash chaining (con llave el tripid)
	public void loadTrips(String tripsFile) 
	{
		try 
		{

			viajesChaining = new HashTableChaining<String, VOTrip>(6007);

			BufferedReader br = new BufferedReader(new FileReader(tripsFile));
			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOTrip viaje = new VOTrip (info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], info[8], info[9]);

				//Cargar viajes a rutas
				VORoute ruta = rutasChaining.get(viaje.route_id());

				if(rutasChaining.contains(viaje.route_id()))
					ruta.agregarViaje(viaje);

				viajesChaining.put(info[2], viaje);

				linea = br.readLine();
			}

			br.close();
		}

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

	}


	//Carga los stoptimes en una lista 
	public void loadStopTimes(String stopTimesFile) 
	{

		try 
		{
			listaStopTimes = new RingList<VOStopTimes>();

			BufferedReader br = new BufferedReader(new FileReader(stopTimesFile));
			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");


				String shape = new String();

				if(info.length==8)
				{
					shape="0";
				}
				else if (info.length==9)
				{
					shape = info[8];
				}


				VOStopTimes stopTime = new VOStopTimes (info[0], info[1], info[2], info[3], info[4],info[5], info[6], info[7], shape);

				VOTrip viaje = viajesChaining.get(stopTime.trip_id());

				//Agregar Viajes a Paradas en hash
				viaje.actualizarTiempoLlegada(stopTime.arrival_time());


				VOStop parada = listaParadasChaining.get(stopTime.stop_id());

				if(listaParadasChaining.contains(stopTime.stop_id()))
					parada.agregarViaje(viaje.trip_id());


				//Agregar stopTimes a Trip en hash
				viaje.agregarStopTimes(stopTime);

				//Agrega parada a viaje
				viaje.agregarParada(parada);


				listaStopTimes.addLast(stopTime);

				linea = br.readLine();
			}

			br.close();
		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 
	}

	//Carga las paradas en una tabla de hash con chaining
	public void loadStops(String stopsFile) 
	{
		try 
		{
			listaParadasChaining = new HashTableChaining<String, VOStop>(6007);


			BufferedReader br = new BufferedReader(new FileReader(stopsFile));
			String linea = br.readLine();
			linea =br.readLine();

			while (null!=linea) 
			{
				String [] info = linea.split(",");

				VOStop parada = new VOStop (info[0], info[1], info[2], info[3], info[4], info[5], info[6], info[7], info[8], "");

				listaParadasChaining.put(info[0], parada);

				linea = br.readLine();
			}

			br.close();

		} 

		catch (Exception e) 
		{
			e.printStackTrace();		
		} 

	}


	//------------------------------------
	// METODO PARA CARGA INFO ESTATICA
	//------------------------------------

	public void cargarGTFS() 
	{
		loadRoutes("./data/routes.txt");
		System.out.println("cargo ruta");

		loadTrips("./data/trips.txt");
		System.out.println("cargo viajes");

		loadStops("./data/stops.txt");
		System.out.println("cargo stops");

		loadStopTimes("./data/stop_times.txt");
		System.out.println("cargo stopstimes");

		System.out.println("cargo estatico");
	}

	//nRutas>=20
	//nViajes>=1
	public void crearGrafoDirigido(int nRutas, int nViajes) throws Exception
	{
		RingList<VOTrip> viajes = new RingList<VOTrip>();
		Iterator<String> iterator =  rutasChaining.keys();
		int contador = 0;

		while(iterator.hasNext() && contador<=nRutas)
		{
			VORoute ruta = rutasChaining.get(iterator.next());
			contador ++;

			RingList<VOTrip> viajesRuta = ruta.darViajes();

			int contador1 = 0;

			for (int i = 0; i < viajesRuta.size() && contador1<=nViajes ; i++) 
			{
				contador1++;

				VOTrip viaje = (VOTrip) viajesRuta.getElement(i);

				viajes.addLast(viaje);
			} 
		}


		grafoDirigido = new DirectedGraph<String, VOStop>();

		//Stoptimes eliminar  
		for (int i = 0; i < viajes.size(); i++) 
		{
			RingList<VOStopTimes> stoptimes = ((VOTrip) viajes.getElement(i)).darStopTimes();
			double distancia1 = 0;
			VOStopTimes primero = null;

			if(!stoptimes.isEmpty())
			{
				primero = (VOStopTimes) stoptimes.getElement(0);
				VOStop parada = listaParadasChaining.get(primero.stop_id());

				distancia1 = Double.parseDouble(primero.shape_dist_traveled());

				grafoDirigido.addVertex(parada.id(), parada);


				stoptimes.removeFirst();
			}


			if(!stoptimes.isEmpty())
			{
				VOStopTimes nuevoPrimero = (VOStopTimes) stoptimes.getElement(0);
				double peso = Double.parseDouble(nuevoPrimero.shape_dist_traveled())-distancia1;
				VOStop parada2 = listaParadasChaining.get(nuevoPrimero.stop_id());

				grafoDirigido.addVertex(parada2.id(), parada2);

				grafoDirigido.addEdge(primero.stop_id(), parada2.id(), peso);
			}

		}

	}


	public int darNumeroDeCC()
	{
		int resp = 0;
		grafoDirigido.crearRaices();
		RingList<Vertex<String,VOStop>> raices = grafoDirigido.darRaices();
		RingList<CC<String,VOStop>> ccs = new RingList<CC<String,VOStop>>();

		for (int i = 0; i < raices.size(); i++) 
		{
			Vertex<String,VOStop> raiz = (Vertex<String, VOStop>) raices.getElement(i);

			CC<String,VOStop> cc = new CC<String,VOStop>(grafoDirigido, raiz);
			cc.build();

			ccs.addLast(cc);
		}

		resp=ccs.size();

		return resp;
	}


	public HashTableChaining<String, Vertex<String,VOStop>> darCiclo() throws Exception
	{
		Cycle<String, VOStop> ciclo = new Cycle(grafoDirigido);
		HashTableChaining<String, Vertex<String,VOStop>> verticesCiclo = ciclo.getCycle();

		return verticesCiclo;
	}



	
	
	

}
